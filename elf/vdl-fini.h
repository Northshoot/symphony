#ifndef VDL_FINI_H
#define VDL_FINI_H

struct VdlList;

void vdl_fini_call (struct VdlList *files);

#endif /* VDL_FINI_H */
